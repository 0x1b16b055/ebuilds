# ebuilds
personal ebuilds of varying quality

many are 'borrowed' from other ebuild repositories (esp guru) and updated/fixed as needed. credits to their original maintainers.

```
# eselect repository add b055 git https://gitlab.com/0x1b16b055/ebuilds
```
